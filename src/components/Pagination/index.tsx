import { Dispatch, SetStateAction } from "react";
import styled from "styled-components";
import { UserResponse } from "types";

const Pagination = ({ item, currentPage, setCurrentPage }: { item: Omit<UserResponse, "content">; currentPage: number; setCurrentPage: Dispatch<SetStateAction<number>> }) => {
    if (item.totalPages < 1) return <></>;
    return (
        <PageWrapper>
            {Array(item.totalPages)
                .fill(null)
                .map((_, i) => (
                    <button key={i} onClick={() => setCurrentPage(i)} data-index={currentPage === i ? i : null}>
                        {i + 1}
                    </button>
                ))}
        </PageWrapper>
    );
};

const PageWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    > button {
        background: #fff;
        border: 1px solid #ddd;
        padding: 4px;
        border-radius: 5px;
        margin: 0 4px;
        &[data-index] {
            font-weight: bold;
        }
    }
`;

export default Pagination;

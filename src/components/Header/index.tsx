import styled from "styled-components";
import { Link } from "react-router-dom";

const Header = () => {
    return (
        <StyledHeader>
            <nav>
                <h1>
                    <Link to="/">React Test</Link>
                </h1>
                <ul>
                    <li>
                        <Link to="/signup">회원등록</Link>
                    </li>
                    {/* <li>로그인</li> */}
                </ul>
            </nav>
        </StyledHeader>
    );
};

const StyledHeader = styled.header`
    > nav {
        width: 1200px;
        margin: 0 auto;
        padding: 10px 0;
        display: flex;
        align-items: center;
        justify-content: space-between;
        > ul {
            display: flex;
            > li {
                margin: 0 10px;
            }
        }
    }
`;

export default Header;

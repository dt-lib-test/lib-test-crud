import { useMutation, useQueryClient } from "@tanstack/react-query";
import { apiUrl } from "App";
import axios from "axios";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import styled from "styled-components";
import { User } from "types";

const UserCard = ({ item }: { item: User }) => {
    const [isEdit, setIsEdit] = useState(false);

    const queryClient = useQueryClient();

    // User 정보 수정 Put 요청
    const { mutate: modifyUser } = useMutation(
        (user: User) => {
            return axios.put(`${apiUrl}/users/${user.id}`, user);
        },
        {
            onSuccess: () => {
                // 요청 성공시
                queryClient.invalidateQueries(["users"]);
                setIsEdit(false);
            },
            onError: (error) => console.log(error), // 요청 실패시
        }
    );

    // User 삭제 Delete 요청
    const { mutate: deleteUser } = useMutation(
        (id: number) => {
            return axios.delete(`${apiUrl}/users/${id}`);
        },
        {
            onSuccess: () => {
                queryClient.invalidateQueries(["users"]);
                setIsEdit(false);
            },
        }
    );

    const {
        register,
        watch,
        setValue,
        formState: { errors },
    } = useForm({ defaultValues: { ...item } });

    useEffect(() => {
        setValue("email", item.email);
        setValue("phone", item.phone);
    }, [item, setValue]);

    return (
        <Container>
            <div>
                <p>{item.id}</p>
                <input type="text" {...register("name")} readOnly={true} />
                <input type="text" {...register("email")} readOnly={!isEdit} />
                <input type="text" {...register("phone")} readOnly={!isEdit} />
                {isEdit && (
                    <div>
                        <button
                            onClick={() => {
                                modifyUser({ ...item, email: watch("email"), phone: watch("phone") });
                            }}
                        >
                            수정
                        </button>
                        <button
                            onClick={() => {
                                if (window.confirm("정말 삭제하시겠습니까?")) {
                                    deleteUser(Number(item.id));
                                }
                            }}
                        >
                            삭제
                        </button>
                    </div>
                )}
            </div>
            {!isEdit && <button onClick={() => setIsEdit(true)}>편집</button>}
        </Container>
    );
};

const Container = styled.div`
    background: #ebebeb;
    padding: 20px;
    border-radius: 5px;
    margin: 20px 0;
    width: 500px;
    position: relative;
    > div {
        display: flex;
        flex-direction: column;
        > input {
            border-radius: 5px;
            margin: 5px 0;
            padding: 4px 5px;
            &:focus {
                outline: none;
            }
            &:read-only {
                background: #ebebeb;
            }
        }
        > div {
            > button {
                background: #757575;
                color: #fff;
                padding: 2px 10px;
                border-radius: 5px;
                margin: 10px 8px 0 0;
            }
        }
    }
    > button {
        position: absolute;
        background: #bdbdbd;
        color: #fff;
        padding: 2px 8px;
        border-radius: 5px;
        top: 10px;
        right: 10px;
        font-size: 13px;
    }
`;

export default UserCard;

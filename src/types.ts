export type User = {
    id?: number;
    name: string;
    code?: string;
    phone: string;
    password: string;
    email: string;
    dateOfCreation?: string;
    dateOfModification?: string;
};

export type Pageable = {
    offset: number;
    pageNumer: number;
    pageSize: number;
    paged: boolean;
    sort: {
        empty: boolean;
        sorted: boolean;
        unsorted: boolean;
    };
    unpaged: boolean;
    size: number;
};

export type UserResponse = {
    content: User[];
    pageable: Pageable;
    totalElements: number;
    totalPages: number;
    number: number;
    numberOfElements: number;
    empty: boolean;
    first: boolean;
    last: boolean;
};

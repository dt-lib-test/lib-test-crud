import { useQuery } from "@tanstack/react-query";
import { apiUrl } from "App";
import axios from "axios";
import Pagination from "components/Pagination";
import UserCard from "components/UserCard";
import { useMemo, useState } from "react";
import styled from "styled-components";
import { User } from "types";

const Main = () => {
    const [currentPage, setCurrentPage] = useState(0);

    // user목록 Get 요청
    const { data, isLoading } = useQuery(["users", currentPage], () => {
        return axios.get(`${apiUrl}/users?page=${currentPage}`);
    });

    const users = useMemo(() => data?.data?.content, [data]);
    const pageable = useMemo(() => data?.data, [data]);

    if (isLoading) return <Section>Loading...</Section>; // data Loading 중일 때
    return (
        <Section>
            <h3>회원 목록</h3>
            <Container>
                {users?.map((item: User) => (
                    <UserCard item={item} key={item.id} />
                ))}
            </Container>
            <Pagination item={pageable} currentPage={currentPage} setCurrentPage={setCurrentPage} />
        </Section>
    );
};

const Section = styled.section`
    width: 1200px;
    margin: 0 auto;
    padding: 40px 0;
    > h3 {
        margin-bottom: 20px;
    }
`;

const Container = styled.div`
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: space-around;
`;

export default Main;

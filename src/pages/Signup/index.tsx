import { useForm } from "react-hook-form";
import styled from "styled-components";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import axios from "axios";
import { apiUrl } from "App";
import { User } from "types";
import { useNavigate } from "react-router-dom";

const Signup = () => {
    const navigate = useNavigate();

    const queryClient = useQueryClient();

    // user 등록 Post 요청
    const { mutate: signup } = useMutation(
        (user: User) => {
            return axios.post(`${apiUrl}/users`, user);
        },
        {
            onSuccess: () => {
                // 성공한 경우
                queryClient.invalidateQueries(["users"]); // 'users' 쿼리키를 가진 데이터를 다시 불러옴
                alert("회원등록이 완료되었습니다.");
                navigate("/");
            },
            onError: (error) => console.log(error),
        }
    );

    const {
        register,
        handleSubmit,
        formState: { errors, isDirty, isValid },
    } = useForm({ mode: "onBlur" });

    const onSubmit = (data: any) => {
        // form 제출
        console.log(data);
        signup({ ...data, code: "default" });
    };

    return (
        <Section>
            <h3>회원가입</h3>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <input
                    type="text"
                    placeholder="이름"
                    {...register("name", {
                        required: true, // 옵션으로 필수값 검증
                    })}
                />
                {/* 입력한 내용에 오류가 있는 경우 사용자에게 알림 */}
                {errors.name && <ErrorMessage>이름을 입력해주세요.</ErrorMessage>}
                <input
                    type="text"
                    placeholder="이메일"
                    {...register("email", {
                        required: true,
                        pattern: /^[a-zA-Z0-9+-_.]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/, // 유효성 검증
                    })}
                />
                {errors.email && <ErrorMessage>정확한 이메일 주소를 입력해주세요.</ErrorMessage>}
                <input
                    type="password"
                    placeholder="비밀번호(4자리 이상)"
                    {...register("password", {
                        required: true,
                        minLength: 4,
                    })}
                />
                {errors.password && <ErrorMessage>4자리 이상 입력해주세요.</ErrorMessage>}
                <input
                    type="text"
                    placeholder="휴대폰번호(숫자만 입력)"
                    {...register("phone", {
                        required: true,
                        pattern: /[0-9]/g,
                    })}
                />
                {errors.phone && <ErrorMessage>숫자만 입력해주세요.</ErrorMessage>}
                {/* 정확하게 양식을 작성하면 버튼 활성화 */}
                <button disabled={!isDirty || !isValid}>가입하기</button>
            </Form>
        </Section>
    );
};

const Section = styled.section`
    width: 1200px;
    margin: 0 auto;
    padding: 40px 20px;
    > h3 {
        text-align: center;
    }
`;

const Form = styled.form`
    width: 520px;
    margin: 20px auto;
    display: flex;
    flex-direction: column;
    input {
        border: 1px solid #ddd;
        padding: 4px 5px;
        border-radius: 5px;
        margin-top: 10px;
    }
    button {
        padding: 4px 0;
        margin-top: 20px;
        background: #000;
        color: #fff;
        border-radius: 5px;
        &:hover {
            background: #555;
        }
        &:disabled {
            background: #bdbdbd;
        }
    }
`;

const ErrorMessage = styled.div`
    font-size: 13px;
    margin: 4px 0;
    padding-left: 5px;
    color: #757575;
`;

export default Signup;

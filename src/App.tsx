import Header from "components/Header";
import Main from "pages";
import Signup from "pages/Signup";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import GlobalStyle from "styles/reset";

export const apiUrl = process.env.REACT_APP_API_URL;

function App() {
    return (
        <BrowserRouter>
            <GlobalStyle />
            <Header />
            <Routes>
                <Route path="/" element={<Main />} />
                <Route path="/signup" element={<Signup />} />
            </Routes>
        </BrowserRouter>
    );
}

export default App;

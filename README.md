
# React Library Test

### 1. 사용한 라이브러리
- React Query   
    → 서버 상태 관리
- React Hook Form   
    → 비제어 컴포넌트 방식으로 form 관리 및 개발

### 2. React Query

#### (1) Query Client 생성
```
const queryClient = new QueryClient()

export default function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Example />
    </QueryClientProvider>
  )
}
```

#### (2) Query 요청
```
const { data } = useQuery(
  queryKey, // 이 Query 요청에 대한 응답 데이터를 캐시할 때 사용할 Unique Key (required)
  fetchFn, // 이 Query 요청을 수행하기 위한 Promise를 Return 하는 함수 (required)
  options, // useQuery에서 사용되는 Option 객체 (optional)
);
```

#### (3) Mutation 요청
```
const { mutate } = useMutation(
  mutationFn, // 이 Mutation 요청을 수행하기 위한 Promise를 Return 하는 함수 (required)
  options, // useMutation에서 사용되는 Option 객체 (optional)
);
```
[https://tech.kakaopay.com/post/react-query-1/](https://tech.kakaopay.com/post/react-query-1/)   
[https://tanstack.com/query/latest/docs/react/overview](https://tanstack.com/query/latest/docs/react/overview)


### 3. React Hook Form
#### (1) useForm hook 사용
```
const { register, handleSubmit, watch, formState: { errors } } = useForm();
```

#### (2) register: 해당 필드 key값 등록, option 객체에서 유효성 검사
```
<input type="text" {...register("example", {
    required: true
})} />
```

[https://tech.inflab.com/202207-rallit-form-refactoring/react-hook-form/](https://tech.inflab.com/202207-rallit-form-refactoring/react-hook-form/)   
[https://react-hook-form.com/](https://react-hook-form.com/)